from .models import Customer
from .serializers import CustomerCreateSerializer, CustomerListSerializer,CustomerDetailSerializer
from rest_framework import generics


class CustomerCreateAPIView(generics.CreateAPIView):
    serializer_class = CustomerCreateSerializer
    queryset = Customer.objects.all()
       
class CustomerListAPIView(generics.ListAPIView):
    serializer_class = CustomerListSerializer
    queryset = Customer.objects.all()


class CustomerDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CustomerDetailSerializer
    queryset = Customer.objects.all()    