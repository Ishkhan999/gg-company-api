from .views import CustomerCreateAPIView, CustomerListAPIView,CustomerDetailAPIView
from django.urls import path

urlpatterns = [
    path('create', CustomerCreateAPIView.as_view()),
    path('all', CustomerListAPIView.as_view()),
    path('detail/<int:pk>', CustomerDetailAPIView.as_view())    
]
