from django.db import models
from PartnerCompanies.models import PartnerCompany


class Customer(models.Model):
    CustomerName = models.CharField(max_length=200)
    Country = models.CharField(max_length=200)
    City = models.CharField(max_length=200)
    Phone = models.IntegerField()
    PartnerCompanyID = models.ForeignKey(PartnerCompany, on_delete=models.CASCADE)
    Rating = models.CharField(max_length=200)
    Bonus = models.CharField(max_length=200)

