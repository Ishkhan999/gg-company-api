from .views import CategoryCreateAPIView, CategoryListAPIView,CategoryDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', CategoryCreateAPIView.as_view()),
    path('all', CategoryListAPIView.as_view()),
    path('detail/<int:pk>', CategoryDetailAPIView.as_view()),
    

]
