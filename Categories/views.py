from .models import Category
from .serializers import CategoryCreateSerializer, CategoryListSerializer,CategoryDetailSerializer
from rest_framework import generics


class CategoryCreateAPIView(generics.CreateAPIView):
    serializer_class = CategoryCreateSerializer
    queryset = Category.objects.all()
    
class CategoryListAPIView(generics.ListAPIView):
    serializer_class = CategoryListSerializer
    queryset = Category.objects.all()

class CategoryDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CategoryDetailSerializer
    queryset = Category.objects.all()