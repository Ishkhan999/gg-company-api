from django.db import models

class Category(models.Model):
    CategoryName = models.CharField(max_length=200)
    Description = models.TextField()
    KmPrice = models.CharField(max_length=200)