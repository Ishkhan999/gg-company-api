from django.apps import AppConfig


class PartnercompaniesConfig(AppConfig):
    name = 'PartnerCompanies'
