from django.contrib import admin 
from .models import PartnerCompany

# Register your models here.
admin.site.register(PartnerCompany)