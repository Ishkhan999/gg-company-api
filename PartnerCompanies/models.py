from django.db import models


class PartnerCompany(models.Model):
    PartnerCompanyName = models.CharField(max_length=200)
    ContactName = models.CharField(max_length=200) 
    Country = models.CharField(max_length=200)  
    City = models.CharField(max_length=200) 
    Address = models.CharField(max_length=200) 
    Phone = models.CharField(max_length=200) 
    Email = models.CharField(max_length=200)
