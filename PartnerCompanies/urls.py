from .views import PartnerCompanyCreateAPIView,PartnerCompanyListAPIView,PartnerCompanyDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', PartnerCompanyCreateAPIView.as_view()),
    path('all', PartnerCompanyListAPIView.as_view()),
    path('detail/<int:pk>', PartnerCompanyListAPIView.as_view()),
    

]
