from .models import PartnerCompany
from .serializers import PartnerCompanyCreateSerializer, PartnerCompanyListSerializer,PartnerCompanyDetailSerializer
from rest_framework import generics


class PartnerCompanyCreateAPIView(generics.CreateAPIView):
    serializer_class = PartnerCompanyCreateSerializer
    queryset = PartnerCompany.objects.all()

class PartnerCompanyListAPIView(generics.ListAPIView):
    serializer_class = PartnerCompanyListSerializer
    queryset = PartnerCompany.objects.all()


class PartnerCompanyDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PartnerCompanyDetailSerializer
    queryset = PartnerCompany.objects.all()