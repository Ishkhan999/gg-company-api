from .models import PartnerCompany
from rest_framework import serializers


class PartnerCompanyCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartnerCompany
        fields = '__all__'


class PartnerCompanyListSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartnerCompany
        fields = '__all__'        



class PartnerCompanyDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartnerCompany
        fields = '__all__'        