from .views import DriverCreateAPIView, DriverListAPIView,DriverDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', DriverCreateAPIView.as_view()),
    path('all', DriverListAPIView.as_view()),
    path('detail/<int:pk>', DriverDetailAPIView.as_view()),
    

]
