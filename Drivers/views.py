from .models import Driver
from .serializers import DriverCreateSerializer,DriverListSerializer,DriverDetailSerializer
from rest_framework import generics


class DriverCreateAPIView(generics.CreateAPIView):
    serializer_class = DriverCreateSerializer
    queryset = Driver.objects.all()

class DriverListAPIView(generics.CreateAPIView):
    serializer_class = DriverListSerializer
    queryset = Driver.objects.all()

class DriverDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DriverDetailSerializer
    queryset = Driver.objects.all()    