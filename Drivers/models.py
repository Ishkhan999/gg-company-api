from django.db import models

class Driver(models.Model):
    FirstName = models.CharField(max_length=200)
    SurName = models.CharField(max_length=200)
    Country = models.CharField(max_length=200)
    City = models.CharField(max_length=200)
    Address = models.CharField(max_length=200)
    PassportID = models.CharField(max_length=200)
    Phone = models.CharField(max_length=200)
    DriverBalance = models.CharField(max_length=200) 
    Rating = models.CharField(max_length=200)
    Bonus = models.CharField(max_length=200)