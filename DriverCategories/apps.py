from django.apps import AppConfig


class DrivercategoriesConfig(AppConfig):
    name = 'DriverCategories'
