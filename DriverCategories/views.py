from .models import DriverCategory
from .serializers import DriverCategoryCreateSerializer, DriverCategoryListSerializer,DriverCategoryDetailSerializer
from rest_framework import generics


class DriverCategoryCreateAPIView(generics.CreateAPIView):
    serializer_class = DriverCategoryCreateSerializer
    queryset = DriverCategory.objects.all()

       
class DriverCategoryListAPIView(generics.ListAPIView):
    serializer_class = DriverCategoryListSerializer
    queryset = DriverCategory.objects.all()

class DriverCategoryDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DriverCategoryDetailSerializer
    queryset = DriverCategory.objects.all()    