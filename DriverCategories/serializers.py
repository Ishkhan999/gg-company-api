from .models import DriverCategory
from rest_framework import serializers


class DriverCategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = DriverCategory
        fields = '__all__'

class DriverCategoryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = DriverCategory
        fields = '__all__'


class DriverCategoryDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = DriverCategory
        fields = '__all__'        