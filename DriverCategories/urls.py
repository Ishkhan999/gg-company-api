from .views import DriverCategoryCreateAPIView, DriverCategoryListAPIView,DriverCategoryDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', DriverCategoryCreateAPIView.as_view()),
    path('all', DriverCategoryListAPIView.as_view()),
    path('detail/<int:pk>', DriverCategoryListAPIView.as_view()),
    

]
