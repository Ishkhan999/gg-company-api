from django.db import models
from Drivers.models import Driver
from Categories.models import Category

class DriverCategory(models.Model):
    DriverID = models.ForeignKey(Driver, on_delete=models.CASCADE)
    CategoryID = models.ForeignKey(Category, on_delete=models.CASCADE)
    # Driver = models.ManyToManyField(Category)
