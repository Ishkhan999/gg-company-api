from django.contrib import admin 
from .models import PriceDetail

# Register your models here.
admin.site.register(PriceDetail)