from .models import PriceDetail
from rest_framework import serializers


class PriceDetailCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PriceDetail
        fields = '__all__'


class PriceDetailListSerializer(serializers.ModelSerializer):
    class Meta:
        model = PriceDetail
        fields = '__all__'


class PriceDetailDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = PriceDetail
        fields = '__all__'

