from .views import PriceDetailCreateAPIView, PriceDetailListAPIView, PriceDetailDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', PriceDetailCreateAPIView.as_view()),
    path('all', PriceDetailListAPIView.as_view()),
    path('detail/<int:pk>', PriceDetailDetailAPIView.as_view()),
    

]
