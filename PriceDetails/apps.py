from django.apps import AppConfig


class PricedetailsConfig(AppConfig):
    name = 'PriceDetails'
