from .models import PriceDetail
from .serializers import PriceDetailCreateSerializer,PriceDetailListSerializer,PriceDetailDetailSerializer
from rest_framework import generics


class PriceDetailCreateAPIView(generics.CreateAPIView):
    serializer_class = PriceDetailCreateSerializer
    queryset = PriceDetail.objects.all()


class PriceDetailListAPIView(generics.ListAPIView):
    serializer_class = PriceDetailListSerializer
    queryset = PriceDetail.objects.all()    

class PriceDetailDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PriceDetailDetailSerializer
    queryset = PriceDetail.objects.all()    
