from .models import OrderDetail
from .serializers import OrderDetailCreateSerializer,OrderDetailListSerializer,OrderDetailDetailSerializer
from rest_framework import generics


class OrderDetailCreateAPIView(generics.CreateAPIView):
    serializer_class = OrderDetailCreateSerializer
    queryset = OrderDetail.objects.all()


class OrderDetailListAPIView(generics.ListAPIView):
    serializer_class = OrderDetailListSerializer
    queryset = OrderDetail.objects.all()    


class OrderDetailDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = OrderDetailDetailSerializer
    queryset = OrderDetail.objects.all()        
