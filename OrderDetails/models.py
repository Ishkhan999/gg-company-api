from django.db import models
from Orders.models import Order
from Vehicles.models import Vehicle


class OrderDetail(models.Model):
    OrderID = models.ForeignKey(Order, on_delete=models.CASCADE)
    VehicleID = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    Quantity = models.IntegerField()
