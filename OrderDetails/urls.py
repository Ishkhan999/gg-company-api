from .views import OrderDetailCreateAPIView,OrderDetailListAPIView,OrderDetailDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', OrderDetailCreateAPIView.as_view()),
    path('all', OrderDetailListAPIView.as_view()),
    path('detail/<int:pk>', OrderDetailDetailAPIView.as_view()),
    

]
