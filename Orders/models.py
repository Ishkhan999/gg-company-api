from django.db import models
from Customers.models import Customer
from Employees.models import Employee
from Drivers.models import Driver
from PriceDetails.models import PriceDetail


class Order(models.Model):
    CustomerID = models.ForeignKey(Customer, on_delete=models.CASCADE)
    EmployeeID = models.ForeignKey(Employee, on_delete=models.CASCADE)
    OrderDatetime = models.DateTimeField(auto_now_add = True)
    DriverID = models.ForeignKey(Driver, on_delete=models.CASCADE)
    PriceDetailID = models.ForeignKey(PriceDetail, on_delete=models.CASCADE,)
        