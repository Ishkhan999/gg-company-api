from .views import OrderCreateAPIView,OrderListAPIView,OrderDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', OrderCreateAPIView.as_view()),
    path('all', OrderListAPIView.as_view()),
    path('detail/<int:pk>', OrderListAPIView.as_view()),
    

]
