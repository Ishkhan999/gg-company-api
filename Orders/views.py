from .models import Order
from .serializers import OrderCreateSerializer,OrderListSerializer,OrderDetailSerializer
from rest_framework import generics


class OrderCreateAPIView(generics.CreateAPIView):
    serializer_class = OrderCreateSerializer
    queryset = Order.objects.all()



class OrderListAPIView(generics.CreateAPIView):
    serializer_class = OrderListSerializer
    queryset = Order.objects.all()    

class OrderDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = OrderDetailSerializer
    queryset = Order.objects.all()    
