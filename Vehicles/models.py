from django.db import models
from Categories.models import Category

class Vehicle(models.Model):
    VMaker =  models.CharField(max_length=200)
    VModel = models.CharField(max_length=200)
    VYear = models.CharField(max_length=200)
    VColor = models.CharField(max_length=200)
    VNumber_Plate = models.CharField(max_length=200)
    CategoryID = models.ForeignKey(Category, on_delete=models.CASCADE)
    VPhoto = models.CharField(max_length=200)  
