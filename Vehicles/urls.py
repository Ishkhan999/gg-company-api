from .views import VehicleCreateAPIView,VehicleListAPIView,VehicleDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', VehicleCreateAPIView.as_view()),
    path('all', VehicleListAPIView.as_view()),
    path('detail/<int:pk>', VehicleDetailAPIView.as_view()),
    

]
