from .models import Vehicle
from .serializers import VehicleCreateSerializer,VehicleListSerializer,VehicleDetailSerializer
from rest_framework import generics


class VehicleCreateAPIView(generics.CreateAPIView):
    serializer_class = VehicleCreateSerializer
    queryset = Vehicle.objects.all()



class VehicleListAPIView(generics.ListAPIView):
    serializer_class = VehicleListSerializer
    queryset = Vehicle.objects.all()



class VehicleDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = VehicleDetailSerializer
    queryset = Vehicle.objects.all()
